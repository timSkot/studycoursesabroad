<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Add any custom values between this line and the "stop editing" line. */



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
ini_set('display_errors', 'Off');
ini_set('display_reporting', E_ALL);
define('WP_DEBUG_DISPLAY', false);
if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}


define('AUTH_KEY',         'vBu7SdqCPKU3t8Utu0zRR9LQEDQwtSdZrba7ew6swRJxzAI1wnWd40rlSvroeuXn9loyPPTSxS8/oMcdDp0ttQ==');
define('SECURE_AUTH_KEY',  'IAJ861TOjuT0AxNYxQiRKnpFxQy32oj8YiVHADqEppIqDZQXuei+i/Pg8PSCZyWqcHTNPB/8UbXekk5mk4ooug==');
define('LOGGED_IN_KEY',    '+nKp0KFYnQyIyVNxdSOJPWKn96wtWzKWD7Vz0jOEQ5cocpT+CpBsCnVVx89GTZ2A9lDPqSPyOAXf39VDfpiKAA==');
define('NONCE_KEY',        'ncsFC7NPLtcQgGXpZLqNp/IJ8TpLbgt1AOn1uj2AvwDL7qGsmz5MhmSKoqCuuvWemaSu5lvUtQCdhBm8yjicag==');
define('AUTH_SALT',        'BJb4JqMZWo7nlVM0fcKVBl3W6Twi5ZINqyyUA6M1mY0NiOGsrvsDNEQmZLW6np8vvS5A+BOwlCykI4GYS986gw==');
define('SECURE_AUTH_SALT', 'lLYi9LTHMzzUZ1znt66HKI/YF0YhXMiFnOykFJ7iGFoJUBf71dK56aekTy8FNfCVFNfwVbqlhYAHUv4XW3Q6rA==');
define('LOGGED_IN_SALT',   'SktKqeI/4yXcVG0Ts2P+LJQzPfQdpQbyKZp1/KFmUdKGZYT0obVfstlgjiNlRjDzKSijVqX+/+i2ohu9mGid5A==');
define('NONCE_SALT',       'RRxxYgIpmBBNqwZChb3qoX/jKW9BuHN9sDlv+CRO8lLmxkK10U4xY4xbjVX8P3dvY3m9v8PFQldIjPBFqw6Lvw==');
define( 'WP_ENVIRONMENT_TYPE', 'local' );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
