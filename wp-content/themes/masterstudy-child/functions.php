<?php
$inc = get_stylesheet_directory().'/inc/';
wp_enqueue_script( 'custom_script', get_stylesheet_directory_uri() . '/assets/js/custom_script.js', 'jquery', '1.0.0', true);
require_once $inc.'/product_meta.php';

add_filter( 'pre_get_posts', 'my_modify_main_query' );
function my_modify_main_query( $query ) {

	// Checking for "city" data
	if( ! isset( $_GET['type'] ) && $_GET['type'] !== 'product' ) return $query;
//	var_dump(explode(",", $_GET['destination']));
//	var_dump($_GET['course_level']);
//	var_dump($_GET['subject_arr']);
//	var_dump($_GET['institution_arr']);
//	exit();
	$meta_query_args = array(
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key' => 'destination',
				'value' => explode(",", $_GET['destination']),
				'compare' => 'IN',
			),
			array(
				'key' => 'course_level',
				'value' => explode(",", $_GET['course_level']),
				'compare' => 'IN',
			),
			array(
				'key' => 'subject',
				'value' => explode(",", $_GET['subject_arr']),
				'compare' => 'IN',
			),
			array(
				'key' => 'institution',
				'value' => explode(",", $_GET['institution_arr']),
				'compare' => 'IN',
			)
		)
	);
	$query->set('meta_query', $meta_query_args);

	return $query; ## <==== This was missing
}