<?php
remove_action( 'admin_init', 'expert_list' );
add_action('admin_init', 'expert_list_child');

function expert_list_child()
{
	if (in_array('woocommerce/woocommerce.php',
		apply_filters('active_plugins', get_option('active_plugins')))) {
		$experts = [
			'no_expert' => 'Choose teacher for course',
		];

		$experts_args = [
			'post_type' => 'teachers',
			'post_status' => 'publish',
			'posts_per_page' => -1,
		];

		$experts_query = new WP_Query($experts_args);

		foreach ($experts_query->posts as $expert) {
			$experts[$expert->ID] = $expert->post_title;
		}

		if (!empty($experts)) {
			STM_PostType::addMetaBox('stm_woo_product_expert',
				__('Course Teacher', 'stm-post-type'), ['product'], '', '', '',
				[
					'fields' => [
						'course_expert' => [
							'label' => __('Teacher', 'stm-post-type'),
							'type' => 'multi-select',
							'options' => $experts,
							'description' => 'Choose Teacher for course',
						],
					],
				]
			);
		}

		STM_PostType::addMetaBox('stm_woo_product_status',
			__('Course Details', 'stm-post-type'), ['product'], '', '', '',
			[
				'fields' => [
					'destination' => [
						'label' => __('Destination', 'stm-post-type'),
						'type' => 'text',
					],
					'course_level' => [
						'label' => __('Course Level', 'stm-post-type'),
						'type' => 'text',
					],
					'subject' => [
						'label' => __('Subject', 'stm-post-type'),
						'type' => 'text',
					],
					'institution' => [
						'label' => __('Institution', 'stm-post-type'),
						'type' => 'text',
					],
					'course_status' => [
						'label' => __('Status', 'stm-post-type'),
						'type' => 'select',
						'options' => [
							'no_status' => __('No Status', 'stm-post-type'),
							'hot' => __('Hot', 'stm-post-type'),
							'special' => __('Special', 'stm-post-type'),
							'new' => __('New', 'stm-post-type'),
						],
					],
					'duration' => [
						'label' => __('Duration', 'stm-post-type'),
						'type' => 'text',
					],
					'lectures' => [
						'label' => __('Lectures', 'stm-post-type'),
						'type' => 'text',
					],
					'video' => [
						'label' => __('Video', 'stm-post-type'),
						'type' => 'text',
					],
					'certificate' => [
						'label' => __('Certificate', 'stm-post-type'),
						'type' => 'text',
					],
				],
			]
		);

		STM_PostType::addMetaBox('stm_woo_product_button_link',
			__('Course Link', 'stm-post-type'), ['product'], '', '', '',
			[
				'fields' => [
					'woo_course_url' => [
						'label' => __('URL', 'stm-post-type'),
						'type' => 'text',
					],
					'woo_course_label' => [
						'label' => __('Button text', STM_POST_TYPE),
						'type' => 'text',
					],
				],
			]
		);
	}
}

add_filter( 'woocommerce_is_purchasable','__return_false',10,2);