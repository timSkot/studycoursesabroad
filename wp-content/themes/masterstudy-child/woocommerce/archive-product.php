<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header(); ?>

<?php
$shop_sidebar_id = stm_option( 'shop_sidebar' );
$enable_shop = stm_option( 'enable_shop', false );
$shop_sidebar_position = stm_option( 'shop_sidebar_position', 'none' );
$content_before = $content_after = $sidebar_before = $sidebar_after = '';
$sidebar_type = '';

// For demo
if( isset( $_GET[ 'sidebar_position' ] ) and $_GET[ 'sidebar_position' ] == 'right' ) {
    $shop_sidebar_position = 'right';
}
elseif( isset( $_GET[ 'sidebar_position' ] ) and $_GET[ 'sidebar_position' ] == 'left' ) {
    $shop_sidebar_position = 'left';
}
elseif( isset( $_GET[ 'sidebar_position' ] ) and $_GET[ 'sidebar_position' ] == 'none' ) {
    $shop_sidebar_position = 'none';
}

if( $shop_sidebar_id ) $shop_sidebar = get_post( $shop_sidebar_id );

if( is_active_sidebar( 'shop' ) ) {
    $shop_sidebar = 'widget_area';
    $shop_sidebar_position = 'right';
}

if( $shop_sidebar_position == 'right' && isset( $shop_sidebar ) ) {
    $content_before .= '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">';
    $content_before .= '<div class="sidebar_position_right">';
    // .products
    $content_after .= '</div>'; // sidebar right
    $content_after .= '</div>'; // col
    $sidebar_before .= '<div class="col-lg-3 col-md-3 hidden-sm hidden-xs">';
    $sidebar_before .= '<div class="sidebar-area sidebar-area-right">';
    // .sidebar-area
    $sidebar_after .= '</div>'; // sidebar area
    $sidebar_after .= '</div>'; // col
    $sidebar_after .= '</div>'; // row
}

if( $shop_sidebar_position == 'left' && isset( $shop_sidebar ) ) {
    $content_before .= '<div class="row">';
    $content_before .= '<div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-12 col-xs-12">';
    $content_before .= '<div class="sidebar_position_left">';
    // .products
    $content_after .= '</div>'; // sidebar right
    $content_after .= '</div>'; // col
    $sidebar_before .= '<div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 hidden-sm hidden-xs">';
    $sidebar_before .= '<div class="sidebar-area sidebar-area-left">';
    // .sidebar-area
    $sidebar_after .= '</div>'; // sidebar area
    $sidebar_after .= '</div>'; // col
    $sidebar_after .= '</div>'; // row
};

// Grid or list
$layout_products = stm_option( 'shop_layout' );
if( isset( $_GET[ 'view_type' ] ) ) {
    if( $_GET[ 'view_type' ] == 'list' ) {
        $layout_products = 'list';
    }
    else {
        $layout_products = 'grid';
    }
}

$destination_arr = [];
$course_level_arr = [];
$subject_arr = [];
$institution_arr = [];

global $wpdb;
$ids = $wpdb->get_col( "SELECT ID FROM {$wpdb->prefix}posts WHERE post_type IN ('product','product_variation')");

if(!empty($ids)){
	foreach ($ids as $id) {
        global $product;
        $destination = get_post_meta($id, 'destination', true);
        $course_level = get_post_meta($id, 'course_level', true);
        $subject = get_post_meta($id, 'subject', true);
        $institution = get_post_meta($id, 'institution', true);
        if($destination) {
            $destination_arr[] = $destination;
        }
        if($course_level) {
            $course_level_arr[] = $course_level;
        }
        if($subject) {
            $subject_arr[] = $subject;
        }
        if($institution) {
            $institution_arr[] = $institution;
        }
	}
}

$destinations = explode(",", $_GET['destination']);
$course_levels = explode(",", $_GET['course_level']);
$subject_arrs = explode(",", $_GET['subject_arr']);
$institution_arrs = explode(",", $_GET['institution_arr']);

$display_type = get_option( 'woocommerce_shop_page_display', '' );

get_template_part( 'partials/title_box' ); ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
	            <?php wc_get_template_part( 'global/helpbar' ); ?>
	            <?php if( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                  <h2 class="archive-course-title"><?php woocommerce_page_title(); ?></h2>
	            <?php endif; ?>
            </div>
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <form class="ms_lms_courses_archive__filter_form" id="products_filter" method="post">
                    <div class="ms_lms_courses_archive__filter_options">
                        <?php
                            if(!empty($destination_arr)):
                        ?>
                        <div class="ms_lms_courses_archive__filter_options_item">
                            <div class="ms_lms_courses_archive__filter_options_item_title active" style="display: flex;">
                                <h3><?php esc_html_e('Destination', 'masterstudy-lms-learning-management-system'); ?></h3>
                            </div>
                            <div class="ms_lms_courses_archive__filter_options_item_content" style="display: block;">
                                <?php foreach (array_unique($destination_arr) as $destination): ?>
                                <div class="ms_lms_courses_archive__filter_options_item_category">
                                    <label class="ms_lms_courses_archive__filter_options_item_checkbox">
                                        <span class="ms_lms_courses_archive__filter_options_item_checkbox_inner">
                                            <input type="checkbox" value="<?php echo $destination; ?>" <?php if (in_array($destination, $destinations)) { echo "checked";} ?> name="destination[]">
                                            <span><i class="fa fa-check"></i></span>
                                        </span>
                                        <span class="ms_lms_courses_archive__filter_options_item_checkbox_label">
                                            <?php echo $destination; ?>
                                        </span>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="ms_lms_courses_archive__filter_options">
                        <?php
			                if(!empty($course_level_arr)):
				        ?>
                        <div class="ms_lms_courses_archive__filter_options_item">
                            <div class="ms_lms_courses_archive__filter_options_item_title active" style="display: flex;">
                                <h3><?php esc_html_e('Сourse level', 'masterstudy-lms-learning-management-system'); ?></h3>
                            </div>
                            <div class="ms_lms_courses_archive__filter_options_item_content" style="display: block;">
                            <?php foreach (array_unique($course_level_arr) as $course_level): ?>
                            <div class="ms_lms_courses_archive__filter_options_item_category">
                                <label class="ms_lms_courses_archive__filter_options_item_checkbox">
                                    <span class="ms_lms_courses_archive__filter_options_item_checkbox_inner">
                                        <input type="checkbox" value="<?php echo $course_level; ?>" <?php if (in_array($course_level, $course_levels)) { echo "checked";} ?> name="course_level[]">
                                        <span><i class="fa fa-check"></i></span>
                                    </span>
                                    <span class="ms_lms_courses_archive__filter_options_item_checkbox_label">
                                    <?php echo $course_level; ?>
                                    </span>
                                </label>
                            </div>
                            <?php endforeach; ?>
                            </div>
                        </div>
			            <?php endif; ?>
                    </div>
                    <div class="ms_lms_courses_archive__filter_options">
                        <?php
                            if(!empty($subject_arr)):
                        ?>
                            <div class="ms_lms_courses_archive__filter_options_item">
                                <div class="ms_lms_courses_archive__filter_options_item_title active" style="display: flex;">
                                    <h3><?php esc_html_e('Subject', 'masterstudy-lms-learning-management-system'); ?></h3>
                                </div>
                                <div class="ms_lms_courses_archive__filter_options_item_content" style="display: block;">
                                <?php foreach (array_unique($subject_arr) as $subject): ?>
                                <div class="ms_lms_courses_archive__filter_options_item_category">
                                    <label class="ms_lms_courses_archive__filter_options_item_checkbox">
                                        <span class="ms_lms_courses_archive__filter_options_item_checkbox_inner">
                                            <input type="checkbox" value="<?php echo $subject; ?>" <?php if (in_array($subject, $subject_arrs)) { echo "checked";} ?> name="subject[]">
                                            <span><i class="fa fa-check"></i></span>
                                        </span>
                                        <span class="ms_lms_courses_archive__filter_options_item_checkbox_label">
                                        <?php echo $subject; ?>
                                        </span>
                                    </label>
                                </div>
                                <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="ms_lms_courses_archive__filter_options">
                        <?php
                            if(!empty($institution_arr)):
                        ?>
                        <div class="ms_lms_courses_archive__filter_options_item">
                            <div class="ms_lms_courses_archive__filter_options_item_title active" style="display: flex;">
                                <h3><?php esc_html_e('Institution', 'masterstudy-lms-learning-management-system'); ?></h3>
                            </div>
                            <div class="ms_lms_courses_archive__filter_options_item_content" style="display: block;">
                            <?php foreach (array_unique($institution_arr) as $institution): ?>
                            <div class="ms_lms_courses_archive__filter_options_item_category">
                                <label class="ms_lms_courses_archive__filter_options_item_checkbox">
                                    <span class="ms_lms_courses_archive__filter_options_item_checkbox_inner">
                                        <input type="checkbox" value="<?php echo $institution; ?>" <?php if (in_array($institution, $institution_arrs)) { echo "checked";} ?> name="institution[]">
                                        <span><i class="fa fa-check"></i></span>
                                    </span>
                                    <span class="ms_lms_courses_archive__filter_options_item_checkbox_label">
                                    <?php echo $institution; ?>
                                    </span>
                                </label>
                            </div>
                            <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="ms_lms_courses_archive__filter_actions">
                        <input type="submit" value="<?php esc_attr_e( 'Show Results', 'masterstudy-lms-learning-management-system' ); ?>" class="ms_lms_courses_archive__filter_actions_button">
                        <a href="/courses" class="ms_lms_courses_archive__filter_actions_reset">
                            <i class="lnr lnr-undo"></i>
                            <span><?php esc_html_e( 'Reset All', 'masterstudy-lms-learning-management-system' ); ?></span>
                        </a>
                    </div>
                </form>
            </div>
            <?php echo wp_kses_post( $content_before ); ?>

            <?php
            do_action( 'woocommerce_archive_description' ); ?>

            <div class="stm_archive_product_inner_grid_content">
                <?php

                if( have_posts() ) : ?>

                    <?php woocommerce_product_loop_start(); ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php if( $layout_products == 'list' ): ?>
                            <?php if( !$enable_shop ): ?>
                                <div class="stm_woo_archive_view_type_list">
                            <?php endif; ?>
                            <?php wc_get_template_part( 'content', 'product-list' ); ?>
                            <?php if( !$enable_shop ): ?>
                                </div>
                            <?php endif; ?>
                        <?php else: ?>

                            <?php wc_get_template_part( 'content', 'product' ); ?>

                        <?php endif; ?>

                    <?php endwhile; // end of the loop. ?>

                    <?php woocommerce_product_loop_end(); ?>

                    <div class="multiseparator <?php echo esc_attr( $layout_products ); ?>"></div>

                    <?php do_action( 'woocommerce_after_shop_loop' ); /* Pagination */ ?>

                <?php elseif( !woocommerce_product_loop() ) : ?>

                    <?php wc_get_template( 'loop/no-products-found.php' ); ?>

                <?php endif; ?>

            </div> <!-- stm_product_inner_grid_content -->
        <?php echo wp_kses_post( $content_after ); ?>

        <?php echo wp_kses_post( $sidebar_before ); ?>
        <?php
        if( isset( $shop_sidebar ) && $shop_sidebar_position != 'none' ) {
            if( $shop_sidebar == 'widget_area' ) {
                dynamic_sidebar( 'shop' );
            }
            else {
                echo apply_filters( 'the_content', $shop_sidebar->post_content );
            }
        }
        ?>
        <?php echo wp_kses_post( $sidebar_after ); ?>

    </div> <!-- container -->

<?php get_footer();