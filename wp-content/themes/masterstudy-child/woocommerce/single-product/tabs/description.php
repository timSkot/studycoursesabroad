<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.0.0
 */

defined( 'ABSPATH' ) || exit;

global $post;
$post_id = $post->ID;
$heading = apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) );

if($post_id) {
	$destination = get_post_meta($post_id, 'destination', true);
	$course_level = get_post_meta($post_id, 'course_level', true);
	$subject = get_post_meta($post_id, 'subject', true);
	$institution = get_post_meta($post_id, 'institution', true);
	$woo_course_url = get_post_meta($post_id, 'woo_course_url', true);
	$woo_course_label = get_post_meta($post_id, 'woo_course_label', true);
}

?>

<?php if ( $heading ) : ?>
	<h2><?php echo esc_html( $heading ); ?></h2>
<?php endif; ?>

<?php the_content(); ?>

<?php if(isset($destination) && $destination): ?>
    <div class="course-meta-field">
        <strong><?php esc_html_e('Destination:', 'masterstudy'); ?></strong>
        <?php echo $destination;?>
    </div>
<?php endif; ?>

<?php if(isset($course_level) && $course_level): ?>
    <div class="course-meta-field">
        <strong><?php esc_html_e('Course Level:', 'masterstudy'); ?></strong>
		<?php echo $course_level;?>
    </div>
<?php endif; ?>

<?php if(isset($subject) && $subject): ?>
    <div class="course-meta-field">
        <strong><?php esc_html_e('Subject:', 'masterstudy'); ?></strong>
		<?php echo $subject;?>
    </div>
<?php endif; ?>

<?php if(isset($institution) && $institution): ?>
    <div class="course-meta-field">
        <strong><?php esc_html_e('Institution:', 'masterstudy'); ?></strong>
		<?php echo $institution;?>
    </div>
<?php endif; ?>

<?php if(isset($woo_course_label) && $woo_course_label): ?>
    <div class="course-meta-field">
        <a target="_blank" class="button product_type_simple" href="<?php if(isset($woo_course_url) && $woo_course_url) {echo $woo_course_url;} ?>"><?php esc_html_e('Visit Website', 'masterstudy');?></a>
    </div>
<?php endif; ?>
