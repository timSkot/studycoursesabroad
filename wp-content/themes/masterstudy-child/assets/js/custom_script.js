(function ($) {
    $(document).ready(function ($) {
        $('#products_filter').submit(function (e) {
            e.preventDefault()
            var destination_val = '';
            var course_level = [];
            var subject_arr = [];
            var institution_arr = [];
            $(this).find('input[name="destination[]"]:checked').each(function(i){
                destination_val += $(this).val() + ',';
            });
            $(this).find('input[name="course_level[]"]:checked').each(function(i){
                course_level += $(this).val() + ',';
            });
            $(this).find('input[name="subject[]"]:checked').each(function(i){
                subject_arr += $(this).val() + ',';
            });
            $(this).find('input[name="institution[]"]:checked').each(function(i){
                institution_arr += $(this).val() + ',';
            });
            console.log(destination_val)
            console.log(course_level)
            console.log(subject_arr)
            console.log(institution_arr)
            window.location.search = '&type=product&destination='+destination_val+'&course_level='+course_level+'&subject_arr='+subject_arr+'&institution_arr='+institution_arr;
        })
    })
})(jQuery);